<!DOCTYPE html>
<html >
<head>
  <!-- Site made with Mobirise Website Builder v4.6.6, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.6.6, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="assets/images/logo4.png" type="image/x-icon">
  <meta name="description" content="Web Page Builder Description">
  <title>Cifrado</title>
  <link rel="stylesheet" href="assets/tether/tether.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="assets/theme/css/style.css">
  <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
  
  
  
</head>
<body>
  <section class="header14 cid-rotHgcZ7nD mbr-fullscreen mbr-parallax-background" id="header14-o">

    
        <div class="mbr-overlay" style="opacity: 0.8; background-color: rgb(5, 93, 51);">
            </div>
    

    <div class="container">
        <div class="media-container-row">

            <div class="mbr-figure" style="width: 30%;">
                <a href="index.php"><img src="assets/images/logo-meso-png.png" alt="Teoria de Sistemas"></a>
                <br>
                <h3 style="color:white">Teoria de Sistemas</h3>
            </div>

            <div class="media-content">
                <h1 class="mbr-section-title pb-3 align-left mbr-white mbr-fonts-style display-1">
                    Cifrador
                </h1>
                
                <div class="mbr-section-text  pb-3 ">
                    
                        <ul>
                            <li><p class="mbr-text align-left mbr-white mbr-fonts-style display-5">JULIO RENE RAMIREZ  / 201008138</p>                                
                            </li>
                            <li><p class="mbr-text align-left mbr-white mbr-fonts-style display-5">EMERSON VELASQUEZ / CARNE 201608058</p>                                
                            </li>                            
                        </ul>                        
                    </p>
                </div>

                <div class="media-container-column" data-form-type="formoid">                 
                    <form action="jramirez1.php" method="post">
                        <div class="form-group">
                            <input type="texto" class="form-control input-sm input-inverse" name="texto" required="" data-form-field="texto" placeholder="Ingrese texto a cifrar" <?php if (isset($_GET['texto'])) {  echo 'value="'.$_GET['texto'].'"'; }?>>
                        </div>
                        <div class="form-group">
                                <input class="btn btn-primary btn-form display-4" type="submit" value = "&nbsp;&nbsp;&nbsp;Cifrar&nbsp;&nbsp;&nbsp;"/><br />
                        </div>
                        <div class="form-group">
                            <textarea class="form-control input-sm input-inverse" name="textocifrado" data-form-field="textocifrado" placeholder="Texto cifrado"><?php if (isset($_GET['hash'])) {  echo $_GET['hash']; }?></textarea>
                        </div>
                    </form>
                    <br>
                    <h1 class="mbr-section-title pb-3 align-left mbr-white mbr-fonts-style display-1">
                    Descifrador
                    </h1>
                    <form  action="jramirez1.php" method="post">
                            <div class="form-group">
                                <input type="textoadescifrar" class="form-control input-sm input-inverse" name="textoadescifrar" required="" data-form-field="textoadescifrar" placeholder="Ingrese texto a descifrar" <?php if (isset($_GET['textoadescifrar'])) {  echo 'value="'.$_GET['textoadescifrar'].'"'; }?>>
                            </div>
                            <div class="form-group">
                                    <input class="btn btn-success btn-form display-4" type="submit" value = " Descifrar "/><br />                        
                            </div>
                            <div class="form-group">
                            <textarea class="form-control input-sm input-inverse" name="textodescifrado" data-form-field="textodescifrado" placeholder="Texto cifrado"><?php if (isset($_GET['textodescifrado'])) {  echo $_GET['textodescifrado']; }?></textarea>                                
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="engine"><a href=""></a></section><section once="" class="cid-rotHsrH4pM" id="footer6-p">

    

    

    <div class="container">
        <div class="media-container-row align-center mbr-white">
            <div class="col-12">
                <p class="mbr-text mb-0 mbr-fonts-style display-7">
                    © Copyright 2019 Jramirez - All Rights Reserved
                </p>
            </div>
        </div>
    </div>
</section>


  <script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/popper/popper.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/smoothscroll/smooth-scroll.js"></script>
  <script src="assets/parallax/jarallax.min.js"></script>
  <script src="assets/theme/js/script.js"></script>
  <!-- <script src="assets/formoid/formoid.min.js"></script> -->
  
  
</body>
</html>