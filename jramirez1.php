<?php 
// include("config.php");

function Mod($a, $b)
{
	return ($a % $b + $b) % $b;
}

function Cifrar($texto, $llave, $direccion)
{
	$largoLlave = strlen($llave);

	for ($i = 0; $i < $largoLlave; ++$i)
		if (!ctype_alpha($llave[$i])) /** Error si no hay caracteres alfabeticos */
			return ""; // Error

	$salida = "";
	$noAlfaNumericoCont = 0;
	$largoTexto = strlen($texto);

	for ($i = 0; $i < $largoTexto; ++$i)
	{
		if (ctype_alpha($texto[$i]))
		{
			$cIsUpper = ctype_upper($texto[$i]);
			$offset = ord($cIsUpper ? 'A' : 'a'); /* Obtenemos Assci  */
			$keyIndex = ($i - $noAlfaNumericoCont) % $largoLlave;
			$k = ord($cIsUpper ? strtoupper($llave[$keyIndex]) : strtolower($llave[$keyIndex])) - $offset;
			$k = $direccion ? $k : -$k;
			$ch = chr((Mod(((ord($texto[$i]) + $k) - $offset), 26)) + $offset);
			$salida .= $ch;
		}
		else
		{
			$salida .= $texto[$i];
			++$noAlfaNumericoCont;
		}
	}

	return $salida;
}

function codificar($input, $key)
{
	return Cifrar($input, $key, true);
}

function descifrar($input, $key)
{
	return Cifrar($input, $key, false);
}

function encrypt($data)
    {
        // Obtenemos la llave privada
        $privKey = openssl_pkey_get_private('file://keys/private.pem');
        // echo 'encontro privada '.$privKey.'<br>';
        if ($privKey)
        {
            $encryptedData = "";
            if (openssl_private_encrypt($data, $encryptedData, $privKey)){
            // echo 'Encrypted: ' . $encryptedData.'<br>';
                return base64_encode($encryptedData);
            }
            else 
            {
                return false;
            }
        }
        else 
            {
                return false;
            }
    }

    function encrypt2($data)
    {
        // Obtenemos la llave privada
        $privKey = openssl_pkey_get_private('file://keys/private.pem');
        // echo 'encontro privada '.$privKey.'<br>';
        if ($privKey)
        {
            $encryptedData = "";
            if (openssl_private_encrypt($data, $encryptedData, $privKey)){
            // echo 'Encrypted: ' . $encryptedData.'<br>';
                return convert_uuencode($encryptedData);
            }
            else 
            {
                return false;
            }
        }
        else 
            {
                return false;
            }
    }
    

    function decrypt($encryptedData)
    {
        $pubKey = openssl_pkey_get_public('file://keys/public.pem');
        // echo 'encontro publica '.$pubKey.'<br>';
        if ($pubKey)
        {
            $decryptedData = "";
            if (openssl_public_decrypt($encryptedData, $decryptedData, $pubKey)){
                return $decryptedData;
            }
            else {
                return false;
            }
            
        }
        else {
            return false;
        }
        
    }

    function inverso($cadena)
    {
        $inversa="";
        for ($i=strlen($cadena)-1; $i >=0 ; $i--) {             
            $inversa = $inversa .substr($cadena,$i,1);
        }
        return $inversa;
    }




if ( ($_SERVER["REQUEST_METHOD"] == "POST") && isset($_POST['texto'])) {



        $texto = $_POST['texto'];

        echo ' texto '.$texto.'<br>'; 

        $texto = inverso($texto);
        
        echo ' inverso '.$texto.'<br>';

        $vigenere = codificar($texto,'meso');

        echo ' vigenere '.$vigenere.'<br>'; 
        $hash = trim(encrypt($vigenere));
        echo $hash;

        header("location: index.php?texto=".$texto.'&hash='.rawurlencode($hash));


    // if (password_verify(hash('sha256', $password, true), $row['bcrypt_pass']))
}

if ( ($_SERVER["REQUEST_METHOD"] == "POST") && isset($_POST['textoadescifrar'])) {



    $textoadescifrar = rawurldecode($_POST['textoadescifrar']);

    echo ' textoadescifrar '.$textoadescifrar.'<br>'; 
    // echo 'hash '.$hash.'<br>';

    $decript = trim(decrypt(base64_decode($textoadescifrar)));
    echo ' antes de vigenere '.$decript.'<br>'; 

    $vigenere = descifrar($decript,'meso');

    echo 'vigenere '.$vigenere.'<br>';

    $texto = inverso($vigenere);
        
        echo ' inverso '.$texto.'<br>';
    header("location: index.php?textoadescifrar=".$textoadescifrar.'&textodescifrado='.rawurlencode($texto));
    // header("location: index.php?texto=".$texto.'&hash='.$hash);


// if (password_verify(hash('sha256', $password, true), $row['bcrypt_pass']))
}

